<?php

class ciclarDatos {

	protected $_ciclo_plantilla = '';
	protected $_datos = array();

	protected $_pre = array();
	protected $_pos = array();

	protected $_ciclo_equal = array();
	protected $_ciclo_equal_plantilla = '';

	protected $conversiones = array();

	static
	protected $tipo_conversiones = array('TEXTOHTML');

	// Constructor
	protected
	function __construct(){
		
	}	

	static
	function getInstance(){
		return new self();
	}

	public
	function setDatos($datos){
		$this->_datos = $datos;
	}

	public
	function addConversion($tipo, $campo){
		
		$tipo = strtoupper($tipo);
		if(!in_array($tipo, self::$tipo_conversiones)){
			return;
		}

		if(isset($this->conversiones[$campo]) 
		   && in_array($tipo, $this->conversiones[$campo]) ){
			return;	
		}

		$this->conversiones[$campo][] = $tipo;
	}

	public
	function setPlantilla($plantilla){
		$this->_ciclo_plantilla = $plantilla;
	}


	//TODO: setPlantillaCampoEqual. Ver para que tome varios campos
	public
	function setPlantillaCampoEqual($campo, $valor, $plantilla){
		$this->_ciclo_equal = array($campo, $valor);
		$this->_ciclo_equal_plantilla =  $plantilla;

	}


	public
	function addPre($texto){
		$this->_pre[] = $texto;
	}

	public
	function addPos($texto){
		$this->_pos[] = $texto;
	}

	public
	function get(){
		$pre = '';
		$cuerpo = '';		
		$pos = '';
		$sumas = array();	// Array para guardar los campos que se tengan que sumar


		$prePlantilla = $this->_pre;
		$posPlantilla = $this->_pos;
		$pre = implode("\n", $prePlantilla);
		$pos = implode("\n", $posPlantilla);

		if(empty($this->_ciclo_plantilla) or empty($this->_datos)){
			//TODO: Poner un throw
			return;
		}

		$datos = $this->_datos;
		$plantilla  = $this->_ciclo_plantilla;

		preg_match_all("/@\[(?<campos>[^\] :]+)]/", $plantilla, $TagCampos);

		preg_match_all("/@\[(?<tipos>suma|constante):(?<valores>[^\] ]+)]/", $pre. $plantilla . $pos, $TagAdicionales);

		$muestra = $datos[0];

		//TODO: Validar que los datos en la plantilla correspondan a los datos del array y de las constantes
		//TODO: Validar las planillas para cada campo

		$nombreCampos 	 = array_keys($muestra);
		$nombreCamposTag = $TagCampos['campos'];
		$nombreTiposTagAdicional = $TagAdicionales['tipos'];
		$nombreValorTagAdicional = $TagAdicionales['valores'];

		/* Para Debug
		echo '<pre>';
		print_r($nombreCampos);
		print_r($nombreCamposTag);
		print_r($nombreTiposTagAdicional);
		print_r($nombreValorTagAdicional);
		echo '</pre>';
		*/

		foreach ($nombreTiposTagAdicional as $key => $value) {
			$valorAdicional = $nombreValorTagAdicional[$key];
			if($value == 'suma'){
				$sumas[$valorAdicional]= 0;
			}
		}
		
		$cicloEqual = $this->_ciclo_equal;
		foreach ($this->_datos as $key => $datosFila) {
			//TODO: Unir las constantes al $datosFila
			
			//TODO: setPlantillaCampoEqual. Ver para que tome varios campos
			if(!empty($cicloEqual) &&
			   array_key_exists($cicloEqual[0], $datosFila) &&
			   $datosFila[ $cicloEqual[0] ] == $cicloEqual[1]){
			   $tmpPlantilla = $this->_ciclo_equal_plantilla;
				
			} else {
				$tmpPlantilla = $plantilla;		// Copiamos la plantilla a un temporal para procesar
			}



			
			foreach($datosFila as $campo =>$valor) // Para cada campo en el array, reemplazar en la plantilla
			{ 

				// TODO: Separar esto en una funcion aparte
				if(array_key_exists($campo, $this->conversiones)){

					foreach ($this->conversiones[$campo] as $tipo_conversion) {
						switch ($tipo_conversion) {
							case 'TEXTOHTML':
								$valor = htmlentities($valor);
								break;
							
						}
					}
				}



				//TODO: Hacer las sumas y reemplazar en el $tmpPlantilla
				if(array_key_exists($campo, $sumas)){
					
					if(is_numeric($valor) && is_numeric($sumas[$campo])){
						$sumas[$campo] += $valor;
					} else {
						$sumas[$campo] = 'error';
					}
					$tmpPlantilla = str_ireplace('@[suma:' . $campo . ']', $sumas[$campo], $tmpPlantilla);
				}
				$tmpPlantilla = str_ireplace('@[' . $campo . ']', $valor, $tmpPlantilla);

			}


			$cuerpo .=$tmpPlantilla;

		}

		
		foreach($sumas as $campo =>$valor){
			$pre = str_ireplace('@[suma:' . $campo . ']', $valor, $pre);
		}
		
		foreach($sumas as $campo =>$valor){
			$pos = str_ireplace('@[suma:' . $campo . ']', $valor, $pos);
		}


		$resultado = $pre . $cuerpo . $pos;
		// Limpiamos el resultado
		// TODO: Remover esto despues de colocar la verificacion de los campos en las plantillas
		preg_match_all("/@\[[^\] ]+]/", $resultado, $tagSinProcesar);
		foreach ($tagSinProcesar[0] as $key => $value) {
			$resultado = str_ireplace($value, '', $resultado);
		}

		return $resultado . "\n";
	}

	public
	function clear(){
		$this->_ciclo_plantilla = '';
		$this->_datos = array();

		$this->_pre = array();
		$this->_pos = array();

		$this->_ciclo_equal = array();
		$this->_ciclo_equal_plantilla = '';

		$this->conversiones = array();
	}

}