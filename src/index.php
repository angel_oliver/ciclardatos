<?php
include('inc' . DIRECTORY_SEPARATOR . 'config.inc.php');
ini_set('display_errors', 1 );



// Datos a procesar
$nombres   = array();
$nombres[] = array('numero' => 10, 
				   'nombre' => 'Jose', 
				   'posicion' =>'<');
$nombres[] = array('numero' => 11, 
				   'nombre' => 'Angel', 
				   'posicion' =>'>');
$nombres[] = array('numero' => 12, 
				   'nombre' => 'Alma', 
				   'posicion' =>'<<<');
$nombres[] = array('numero' => 13, 
				   'nombre' => 'Estela', 
				   'posicion' =>'<<>');
$nombres[] = array('numero' => 14, 
				   'nombre' => 'Monica', 
				   'posicion' =>'<>>');
$nombres[] = array('numero' => 15, 
				   'nombre' => 'Lourdes', 
				   'posicion' => '>>>');


$a = ciclarDatos::getInstance();

$a->setDatos($nombres);

$a->addPre('<div>Suma de los valores: @[suma:numero] </div>');
$a->addPre('<select>');
$a->setPlantilla('<option value="@[numero]">@[numero] - @[nombre]</option>');
$a->setPlantillaCampoEqual('numero', 13,'<option value="@[numero]" selected="selected">@[numero] - @[nombre] </option>');
$a->addPos('</select>');

echo $a->get();

$a->clear();

$a->addConversion('textoHtml', 'posicion');

$a->addPre('<table>');
$a->addPre('<thead>');
$a->addPre('<td>numero</td>');
$a->addPre('<td>nombre</td>');
$a->addPre('<td>posicion</td>');
$a->addPre('</thead>');
$a->addPre('<tbody>');

$a->setDatos($nombres);
$a->setPlantilla('
	<tr>
		<td>@[numero]</td>
		<td>@[nombre]</td>
		<td>@[posicion]</td>
	</tr>
	');

$a->addPos('</tbody>');
$a->addPos('</table>');

echo $a->get();