<?php




function __autoload($class){
	$baseDir = realpath(__DIR__);
	
	include_once($baseDir . DIRECTORY_SEPARATOR . 
				 '..' . DIRECTORY_SEPARATOR . 
				 'class' . DIRECTORY_SEPARATOR . 
				 $class . '.class.php');
	
}